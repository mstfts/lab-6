#include <iostream>
#include <bitset>
#include <cstdint>

using namespace std;

int main(int argc, char *argv[])
{

    int c = 10;
    int d = 20;

    bitset<4> a{0b0101};
    bitset<4> b{0b1011};

    cout << "OR\n";
    cout << "a: " << a << "\nb: " << b << "\n";
    cout << (a | b) << "\n";

    cout << "NOT\n";
    cout << "a: " << a << "\n";
    cout << "~a: " << (~a) << "\n";

    cout << "XOR\n";
    cout << "a: " << a << "\nb: " << b << "\n";
    cout << (a ^ b) << "\n";

    cout << "AND\n";
    cout << "a: " << a << "\nb: " << b << "\n";
    cout << (a & b) << "\n";

    cout << "SHIF LEFT\n";
    cout << "a: " << a << "\n";
    cout << "<<a: " << (a << 3) << "\n";

    cout << "SHIF RIGHT\n";
    cout << "b: " << b << "\n";
    cout << ">>b: " << (b >> 3) << "\n";

    cout << "--------------------------\n" << "SWAP \n";

    cout << "c: " << c << "\nd: " << d << "\n";

    c = c ^ d;
    d = c ^ d;
    c = c ^ d;

    cout << "c: " << c << "\nd: " << d << "\n";

    return 0;
}
